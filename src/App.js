import React, { useState, useEffect } from "react";
import axios from "axios";
import Navigation from "./Navigation/Nav";
import Products from "./Products/Products";
import products from "./db/data";
import Recommended from "./Recommended/Recommended";
import Sidebar from "./Sidebar/Sidebar";
import Card from "./components/Card";
import "./index.css";

const App = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [authenticated, setAuthenticated] = useState(false);

  useEffect(() => {
    // Check user authentication status on component mount
    // You might want to fetch this information from your backend
    // For simplicity, we'll use a mock function here
    checkAuthenticationStatus();
  }, []);

  const checkAuthenticationStatus = () => {
    // Mock function, replace with actual authentication logic
    // For example, you might make a request to your backend to check if the user is authenticated
    const isAuthenticated = localStorage.getItem("token") ? true : false;
    setAuthenticated(isAuthenticated);
  };

  const handleLogin = async (event) => {
    event.preventDefault();
    try {
      // Mock API endpoint, replace with your actual login endpoint
      const response = await axios.post("https://dummyjson.com/auth/login", {
        username: username,
        password: password,
      });

      const token = response.data.token;
      localStorage.setItem("token", token);
      setAuthenticated(true);
    } catch (error) {
      console.error("Login failed:", error);
    }
  };

  const handleLogout = () => {
    // Mock function, replace with actual logout logic
    // For example, you might make a request to your backend to log the user out
    localStorage.removeItem("token");
    setAuthenticated(false);
  };
  const [selectedCategory, setSelectedCategory] = useState(null);
  const [query, setQuery] = useState("");

  const handleInputChange = (event) => {
    setQuery(event.target.value);
  };

  const filteredItems = products.filter(
    (product) => product.title.toLowerCase().indexOf(query.toLowerCase()) !== -1
  );

  const handleChange = (event) => {
    setSelectedCategory(event.target.value);
  };

  const handleClick = (event) => {
    setSelectedCategory(event.target.value);
  };

  function filteredData(products, selected, query) {
    let filteredProducts = products;

    if (query) {
      filteredProducts = filteredItems;
    }

    if (selected) {
      filteredProducts = filteredProducts.filter(
        ({ category, color, company, newPrice, title }) =>
          category === selected ||
          color === selected ||
          company === selected ||
          newPrice === selected ||
          title === selected
      );
    }
    return filteredProducts.map(
      ({ id, img, title, star, reviews, prevPrice, newPrice }) => (
        <Card
          key={id}
          img={img}
          title={title}
          star={star}
          reviews={reviews}
          prevPrice={prevPrice}
          newPrice={newPrice}
        />
      )
    );
  }

  const result = filteredData(products, selectedCategory, query);

  return (
    <div className="App">
      {authenticated ? (
        <>
          <h1 className="heading">Welcome, Bro!</h1>
          <button className="btn btn-primary" onClick={handleLogout}>
            Logout
          </button>
          <Sidebar handleChange={handleChange} />
          <Navigation query={query} handleInputChange={handleInputChange} />
          <Recommended handleClick={handleClick} />
          <Products result={result} />
        </>
      ) : (
        <form className="thon" onSubmit={handleLogin}>
          <h1 className="heading">Welcome to Thon Store!</h1>
          <label className="p-1">
            Username:
            <input
              className="bg-me btn-primary"
              type="text"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </label>
          <br />
          <label className="p-1">
            Password:
            <input
              className="bg-me btn-primary"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </label>
          <br />
          <button className="btn btn-primary" type="submit">
            Login
          </button>
        </form>
      )}
    </div>
  );
};

export default App;

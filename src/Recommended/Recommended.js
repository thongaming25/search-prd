import Button from "../components/Button";
import "./Recommended.css";

const Recommended = ({ handleClick }) => {
  return (
    <>
      <div>
        <h2 className="recommended-title">Pobpular</h2>
        <div className="recommended-flex">
          <Button onClickHandler={handleClick} value="" title="All Products" />
          <Button
            onClickHandler={handleClick}
            value="Macbook"
            title="Macbook"
          />
          <Button onClickHandler={handleClick} value="Iphone" title="IPhone" />
          <Button
            onClickHandler={handleClick}
            value="Apple Watch"
            title="Apple Watch"
          />
          <Button onClickHandler={handleClick} value="Imac" title="Imac" />
        </div>
      </div>
    </>
  );
};

export default Recommended;
